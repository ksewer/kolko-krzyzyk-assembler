;dyrektywa ASSUME przyporządkowywuje logicznie nazwy do segmentow. np. dzieki temu program sam bedzie szukal etykiet we wszystkich segmentach programu

ekran segment at 0B800h
	ek dB ?
	atr db ?
ekran ends

STOSIK SEGMENT stack	;rezerwacja pamieci dla stosu o wielkosci 100h
db 100h dup (?)
STOSIK ENDS

;ASSUME cs:CODE,ds:DANE,ss:STOSIK,es:ekran;

;stale programu 
;ok equ 0
dos equ 21h					; equ EQU przyporządkowuje wartość 21h do zmiennej o nazwie dos

;poczatek segmentu kodu
CODE SEGMENT 
ASSUME cs:CODE,ds:DANE,ss:STOSIK,es:ekran;			; dyrektywa assume przyporządkowywuje w tym wypadku do rejestru cs segment CODE, do ds segment DANE, do ss STOS oraz do es pamięć ekranu
;zmienne
tekst1 db 'Gra kolko krzyzyk',13,10,36 ;13,10 - znak entera
pole1 db "1$" 
pole2 db "2$"
pole3 db "3$" 
pole4 db "4$" 
pole5 db "5$" 
pole6 db "6$" 
pole7 db "7$" 
pole8 db "8$" 
pole9 db "9$" 
zmienna_x db "X" 
zmienna_o db "O" 
nowa_linia db 13,10,36 
aktualny_gracz db "X$"
licznik_ruchow db 0


;etykieta start jest punktem startowym programu
Start:

mov ax,0000h;linijki te odpowiadaja za zresetowanie NUMLOCK
mov ds,ax
mov bx,0417h
mov al,0edh
out 60h,al

mov ax,dane ;deklaracja danych
mov ds,ax
mov ax,stosik ;deklaracja stosu
mov ss,ax 
mov sp, stosik
	
	
	MOV	AH,0			;wybór trybu pracy monitora
	MOV	AL,3			;text 80x25 kolor
	INT	10H				;obsługa ekranu
	mov	ax,seg dane 	;ładujemy rejestry segmentowe DS i ES
	mov	ds,ax
	mov ax,seg ekran 	;pamięć ekranu do ES
	mov	es,ax
	
	
rysuj: 
;---------------------------------------------
;pisanie po pamięci ekranu napisu TURA GRACZA:
   mov si, 130+80*2*0
	mov	ek[si],"T"
	mov si, 132+80*2*0
	mov	ek[si],"U"
	mov si, 134+80*2*0
	mov	ek[si],"R"
	mov si, 136+80*2*0
	mov	ek[si],"A"
	mov si, 138+80*2*0
	mov	ek[si]," "
	mov si, 140+80*2*0
	mov	ek[si],"G"
	mov si, 142+80*2*0
	mov	ek[si],"R"
	mov si, 144+80*2*0
	mov	ek[si],"A"
	mov si, 146+80*2*0
	mov	ek[si],"C"
	mov si, 148+80*2*0
	mov	ek[si],"Z"
	mov si, 150+80*2*0
	mov	ek[si],"A"
	mov si, 152+80*2*0
	mov	ek[si],":"
	
;---------------------------------------------
;rysowanie planszy na której toczy się główna rozgrywka
mov	cx,24
mov si, 60
plansza_pion_L_L:	
	add si,160
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,5000
mov ah,86h		; funkcja ktora nic nie robi przez CX:DX mikrosekund, odpowiada za animacje
int 15h
pop cx
pop dx
	loop plansza_pion_L_L
	
mov	cx,24
add si,162
plansza_pion_L_P:	
	sub si,160
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,5000
mov ah,86h		; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx
	loop plansza_pion_L_P
	
sub si,120
mov	cx,24
plansza_pion_P_L:	
	add si,160
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,5000
mov ah,86h		; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx
	loop plansza_pion_P_L
	
mov	cx,24
add si,162
plansza_pion_P_P:	
	sub si,160
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,5000
mov ah,86h		; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx
	loop plansza_pion_P_P

add si,1034

mov	cx,64
plansza_poziom_G:	
	add si,2
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,5000
mov ah,86h		; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx
	loop plansza_poziom_G
	
add si,1442
	mov	cx,64
plansza_poziom_D:	
	sub si,2
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,5000
mov ah,86h		; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx
	loop plansza_poziom_D
	
;---------------------------------------------
; ten fragment kodu odpowiada za wypisywanie numerków na planszy głównej
	
	mov si, 20+160*18
	mov bl,'1'
	mov	ek[si],bl
	mov si, 42+22+160*18
	mov bl,'2'
	mov	ek[si],bl
	mov si, 42+42+22+160*18
	mov bl,'3'
	mov	ek[si],bl
	mov si, 20+160*9
	mov bl,'4'
	mov	ek[si],bl
	mov si, 44+20+160*9
	mov bl,'5'
	mov	ek[si],bl
	mov si, 44+42+20+160*9
	mov bl,'6'
	mov	ek[si],bl
	mov si, 20+160*1
	mov bl,'7'
	mov	ek[si],bl
	mov si, 44+20+160*1
	mov bl,'8'
	mov	ek[si],bl
	mov si, 44+42+20+160*1
	mov bl,'9'
	mov	ek[si],bl
	jmp ruch
	
jmp koniec

;---------------------------------------------
ruch:
petla:	
	mov si, 154+80*2*0			; odpowiada za wyswietlanie tury aktualnego gracza, push dx i pop dx uzylem aby nie zmienic ich wartosci w innym miejscu niepotrzebnie 
	push dx
	mov dl,[aktualny_gracz]
	mov	ek[si],dl
	pop dx
	
;---------------------------------------------
;odczytuje aktualnie wciśnięty klawisz

    mov  AH, 08h		; funkcja 08h - odczyt znaku		
	int  21h			;kod znaku zwracany jest w AL
	cmp  AL, 1Bh		;sprawdzenie czy naciśnięto ESC	
	je   koniec			;jeśli ESC to koniec działania pętli
	
;---------------------------------------------
;sprawdzanie jaki klawisz został wciśnięty oraz wywołanie odpowiedniej procedury odpowiadającej za ruch na danym polu

	
	mov dl,[aktualny_gracz]
	cmp AL,31H			;dla 1
	je ruch_pole_1
	cmp AL,32H			;dla 2
	je ruch_pole_2
	cmp AL,33H			;dla 3
	je ruch_pole_3
	cmp AL,34H			;dla 4
	je ruch_pole_4
	cmp AL,35H			;dla 5
	je ruch_pole_5
	cmp AL,36H			;dla 6
	je ruch_pole_6
	cmp AL,37H			;dla 7
	je ruch_pole_7
	cmp AL,38H			;dla 8
	je ruch_pole_8
	cmp AL,39H			;dla 9
	je ruch_pole_9
	mov  DL, AL			;jeśli nie ESC to znak do DL
	mov  AH,  02h		;funkcja 02h - wyświetlenie znaku	;
	int  21h			;kod przekazywany w DL
	jmp  petla		
	

;---------------------------------------------


;KONIEC PROGRAMU
koniec:
mov ax,4C00h
int DOS 				;skorzystanie ze zmiennej dos i wywolanie funckji int 21h

ruch_pole_1:
call rruch_pole_1
ruch_pole_2:
call rruch_pole_2
ruch_pole_3:
call rruch_pole_3
ruch_pole_4:
call rruch_pole_4
ruch_pole_5:
call rruch_pole_5
ruch_pole_6:
call rruch_pole_6
ruch_pole_7:
call rruch_pole_7
ruch_pole_8:
call rruch_pole_8
ruch_pole_9:
call rruch_pole_9
	
		
		
;---------------------------------------------
;procedury odpowiadajace za sprawdzeniu czy pole jest zajete oraz wywolaniu funkcji rysujacej X/O 


rruch_pole_1 PROC NEAR
mov bl,[pole1]				;dwie pierwsze linijki w kodzie ładują do bl zawartość zmiennej pole*, i wywoluja procedure sprawdzajaca czy dane pole jest wolne 
call sprawdz_czy_wolne
mov [pole1],dl				;jeżeli procedura sprawdz_czy_wolne nie wykryje, ze pole jest zajete, do pola jest ladowana zawartosc dl, w ktorej znajduje sie znak aktualnego gracza zaladowany w kodzie pod etykieta ruch
mov si, 2912				; ta linijka ustawia si na miejsce na ekranie, od kad procedury rysujace O/X maja zaczac rysowac
call sprawdzenie_rysowania	; wywoluje sprawdzenie rysowania, ktore zaleznie od zawartosci rejestru dl rysuje we wskazanym linijke wyzej miejscu X badz O
rruch_pole_1 endp

rruch_pole_2 proc NEAR
mov bl,[pole2]
call sprawdz_czy_wolne
mov [pole2],dl
mov si, 2954
call sprawdzenie_rysowania
rruch_pole_2 endp

rruch_pole_3 proc NEAR
mov bl,[pole3]
call sprawdz_czy_wolne
mov [pole3],dl
mov si, 2998
call sprawdzenie_rysowania
rruch_pole_3 endp

rruch_pole_4 proc NEAR
mov bl,[pole4]
call sprawdz_czy_wolne
mov [pole4],dl
mov si, 1472
call sprawdzenie_rysowania
rruch_pole_4 endp

rruch_pole_5 proc NEAR
mov bl,[pole5]
call sprawdz_czy_wolne
mov [pole5],dl
mov si, 1514
call sprawdzenie_rysowania
rruch_pole_5 endp

rruch_pole_6 proc NEAR
mov bl,[pole6]
call sprawdz_czy_wolne
mov [pole6],dl
mov si, 1558
call sprawdzenie_rysowania
rruch_pole_6 endp

rruch_pole_7 proc NEAR
mov bl,[pole7]
call sprawdz_czy_wolne
mov [pole7],dl
mov si, 32
call sprawdzenie_rysowania
rruch_pole_7 endp

rruch_pole_8 proc NEAR
mov bl,[pole8]
call sprawdz_czy_wolne
mov [pole8],dl
mov si, 74
call sprawdzenie_rysowania
rruch_pole_8 endp

rruch_pole_9 proc NEAR
mov bl,[pole9]
call sprawdz_czy_wolne
mov [pole9],dl
mov si, 118
call sprawdzenie_rysowania
rruch_pole_9 endp

;---------------------------------------------
;sprawdza czy ma byc wywolana funkcja odpowiadajaca za rysowanie x czy rysowanie O
sprawdzenie_rysowania proc near
cmp dl,'X'							;jezeli w dl znajduje sie X, jest skok do etykiety rys_x w ktorej znajduje sie wywolanie procedury rysujacej X na planszy
je rys_x
cmp dl,'O'							; analogicznie to samo co wyzej
je rys_o

ret									
;---------------------------------------------

rys_o:
call rysowanie_o_male
jmp dalej
rys_x:
call rysowanie_x
dalej:
call sprawdzenie
jmp ruch
ret
sprawdzenie_rysowania endp	
	
	
	
;---------------------------------------------
; pierwsze sprawdzenie czy pola są takie same
sprawdzenie proc near
mov dl, [aktualny_gracz]
cmp dl,"X"
je zmien_na_o
cmp dl,"O"
je zmien_na_x

zmien_na_o:
mov dl,"O"
mov [aktualny_gracz],dl
jmp sprawdzenie_dalej
zmien_na_x:
mov dl,"X"
mov [aktualny_gracz],dl
sprawdzenie_dalej:
mov al, [licznik_ruchow]
add al,1
mov [licznik_ruchow],al

sprawdzenie_12:
	mov bl,[pole2]
	cmp [pole1],bl
	je sprawdzenie_23
	
sprawdzenie_45:
	mov bl,[pole5]
	cmp [pole4],bl
	je sprawdzenie_56
	
sprawdzenie_78:
	mov bl,[pole8]
	cmp [pole7],bl
	je sprawdzenie_89
	
sprawdzenie_14:
	mov bl,[pole4]
	cmp [pole1],bl
	je sprawdzenie_47
	
sprawdzenie_25:
	mov bl,[pole5]
	cmp [pole2],bl
	je sprawdzenie_58
	
sprawdzenie_36:
	mov bl,[pole6]
	cmp [pole3],bl
	je sprawdzenie_69
	
sprawdzenie_15:
	mov bl,[pole5]
	cmp [pole1],bl
	je sprawdzenie_59
	
sprawdzenie_35:
	mov bl,[pole5]
	cmp [pole3],bl
	je sprawdzenie_57

sprawdzenie_remisu:
	mov al,[licznik_ruchow]
	cmp al,9
	je remis
	
ret
sprawdzenie endp	

remis proc near
call remis_proc
remis endp
;---------------------------------------------
;sprawdzenie czy drugie pola sa takie same
sprawdzenie_23 proc near
	cmp [pole3],bl
	je wyswietlenie_wygranej_ekran
	jmp sprawdzenie_45
sprawdzenie_23 endp

sprawdzenie_56 proc near
	cmp [pole6],bl
	je wyswietlenie_wygranej_ekran
	jmp sprawdzenie_78
sprawdzenie_56 endp
	
sprawdzenie_89 proc near
	cmp [pole9],bl
	je wyswietlenie_wygranej_ekran
	jmp sprawdzenie_14
sprawdzenie_89 endp
	
sprawdzenie_47 proc near
	cmp [pole7],bl
	je wyswietlenie_wygranej_ekran
	jmp sprawdzenie_25
sprawdzenie_47 endp
	
sprawdzenie_58 proc near
	cmp [pole8],bl
	je wyswietlenie_wygranej_ekran
	jmp sprawdzenie_36
sprawdzenie_58 endp
	
sprawdzenie_69 proc near
	cmp [pole9],bl
	je wyswietlenie_wygranej_ekran
	jmp sprawdzenie_15
sprawdzenie_69 endp
	
sprawdzenie_59 proc near
	cmp [pole9],bl
	je wyswietlenie_wygranej_ekran
	jmp sprawdzenie_35
sprawdzenie_59 endp
	
sprawdzenie_57 proc near
	cmp [pole7],bl
	je wyswietlenie_wygranej_ekran
	jmp sprawdzenie_remisu
	
sprawdzenie_57 endp

;---------------------------------------------
; wyswietlenie wygranej 
wyswietlenie_wygranej_ekran proc near
assume cs:code,ds:dane,es:ekran
mov ax,3				;czyszczenie ekranu
int 10h
	mov	ax,seg dane 	;ładujemy rejestry segmentowe DS i ES
	mov	ds,ax
	mov ax,seg ekran 	;pamięć ekranu do ES
	mov	es,ax
	MOV	AH,0			;wybór trybu pracy monitora
	MOV	AL,3			;text 80x25 kolor
	INT	10H				;obsługa ekranu	

    mov si, 40+80*2*3
	mov	ek[si],"W"
	mov si, 50+80*2*4
	mov	ek[si],"Y"
	mov si, 60+80*2*5
	mov	ek[si],"G"
	mov si, 70+80*2*6
	mov	ek[si],"R"
	mov si, 80+80*2*7
	mov	ek[si],"A"
	mov si, 90+80*2*8
	mov	ek[si],"N"
	mov si, 100+80*2*9
	mov	ek[si],"A"

	kol EQU 0*2
	line=160 ;80*2
	nr=3
	
	
	
mov	cx,40
nr=nr+9
mov si, kol+line*nr 
add si, 30
poziom_g:	
	add si,2
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx
	loop poziom_g

	
	
	mov cx,11
pion_p:
add si,160
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
	loop pion_p
	
		mov cx,40
poziom_d:
sub si, 2
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
	loop poziom_d

		mov cx,11
pion_l:
sub si,160
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
	loop pion_l
	add si, 352
mov bl,[aktualny_gracz]					
cmp bl,"O"				;jeżeli w bl znajduje się X to następuje przeskok do rysowanie_x
je wrysowanie_x

cmp bl,"X"				;jeżeli w bl znajduje się O to następuje przeskok do rysowanie_o
je rysowanie_o

	jmp rysowanie_planszy_koncowej;skok do rysowania planszy koncowej
wyswietlenie_wygranej_ekran endp


wrysowanie_x proc near ;x rysujacy sie na planszy wygranej
mov cx,6
wrysuj_x_p:
add si, 162
	mov atr[si],74
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
	loop wrysuj_x_p
	
	mov cx,6
	sub si,10
wrysuj_x_l:
	mov atr[si],74
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
sub si, 158
	loop wrysuj_x_l
jmp rysowanie_planszy_koncowej
wrysowanie_x endp

rysowanie_o proc near	;rysowanie O wygranej

mov cx,8
rysuj_O_g:
add si, 2
	mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
	loop rysuj_O_g
	
add si,162
mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	

add si,2
mov cx,4
rysuj_O_p:
add si, 160
	mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
	loop rysuj_O_p
	
add si,158
mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	


mov cx,8
add si,158
rysuj_O_d:

	mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
sub si, 2
	loop rysuj_O_d
	
sub si,160
mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	

mov cx,4
sub si,162
rysuj_O_l:
	mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
sub si, 160
	loop rysuj_O_l

add si,2
mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx		
	jmp rysowanie_planszy_koncowej
jmp koniec
rysowanie_o endp

rysowanie_x proc near 	;prodecura rysujaca x na plaszy rozgrywki

mov cx,6
rysuj_x_p:
add si, 162
	mov atr[si],74
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
	loop rysuj_x_p
	
	mov cx,6
	sub si,10
rysuj_x_l:
	mov atr[si],74
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
sub si, 158
	loop rysuj_x_l
ret
rysowanie_x endp

rysowanie_o_male proc near ;procedura rysujaca o na planszy rozgrywki

add si, 160
mov cx,8
mrysuj_O_g:
add si, 2
	mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
	loop mrysuj_O_g
	
add si,162
mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	

add si,2
mov cx,2
mrysuj_O_p:
add si, 160
	mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
	loop mrysuj_O_p
	
add si,158
mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	


mov cx,8
add si,158
mrysuj_O_d:

	mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
sub si, 2
	loop mrysuj_O_d
	
sub si,160
mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	

mov cx,2
sub si,162
mrysuj_O_l:
	mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
sub si, 160
	loop mrysuj_O_l

add si,2
mov atr[si],42
push dx;
push cx;
mov cx,0;
mov dx,40000	
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx		
ret
rysowanie_o_male endp
;---------------------------------------------
; procedura odpowiadajaca za rysowanie planszy koncowej
rysowanie_planszy_koncowej proc near ;rysowanie malej plaszy w gornym prawym rogu z ostateczna sytuacja na planszy po wygranej

mov si,610
mov cx,13
plansza_g:
add si, 2
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
	loop plansza_g
	
add si,320
mov cx,13
plansza_d:
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
sub si, 2
	loop plansza_d
	
mov	cx,5
add si,168
plansza_l:
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
sub si,160
	loop plansza_l
add si,802
mov	cx,5
plansza_l2:
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
sub si,160
	loop plansza_l2
	
mov	cx,5
add si,170
plansza_p:
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
add si,160
	loop plansza_p
	
	mov	cx,5
sub si,802
plansza_p2:
	mov atr[si],16
push dx;
push cx;
mov cx,0;
mov dx,40000
mov ah,86h				; funkcja ktora nic nie robi przez CX:DX mikrosekund
int 15h
pop cx
pop dx	
add si,160
	loop plansza_p2
	
	mov dl,[pole7]
	mov si, 134+80*2*2
	mov	ek[si],dl
	
	mov dl,[pole8]
	mov si, 144+80*2*2
	mov	ek[si],dl
	
	mov dl,[pole9]
	mov si, 154+80*2*2
	mov	ek[si],dl
	
	mov dl,[pole4]
	mov si, 134+80*2*4
	mov	ek[si],dl
	
	mov dl,[pole5]
	mov si, 144+80*2*4
	mov	ek[si],dl
	
	mov dl,[pole6]
	mov si, 154+80*2*4
	mov	ek[si],dl
	
	mov dl,[pole1]
	mov si, 134+80*2*6
	mov	ek[si],dl
	
	mov dl,[pole2]
	mov si, 144+80*2*6
	mov	ek[si],dl
	
	mov dl,[pole3]
	mov si, 154+80*2*6
	mov	ek[si],dl
	
jmp koniec
rysowanie_planszy_koncowej endp
;---------------------------------------------
;wyswietlanie remisu
remis_proc proc near

	MOV	AH,0			;wybór trybu pracy monitora
	MOV	AL,3			;text 80x25 kolor
	INT	10H				;obsługa ekranu	

	mov si, 50+80*2*4
	mov	ek[si],"R"
	mov si, 60+80*2*5
	mov	ek[si],"E"
	mov si, 70+80*2*6
	mov	ek[si],"M"
	mov si, 80+80*2*7
	mov	ek[si],"I"
	mov si, 90+80*2*8
	mov	ek[si],"S"
	call rysowanie_planszy_koncowej
	
jmp koniec
remis_proc endp
;---------------------------------------------
;procedura odpowiadajaca za sprawdzenie czy pole jest wolne
sprawdz_czy_wolne proc near

cmp bl,"X"
je ruch_skok
cmp bl,"O"
je ruch_skok

ret

ruch_skok:
jmp ruch

sprawdz_czy_wolne endp

CODE ENDS

DANE SEGMENT
;tu umieszczamy dane i zmienne programu
STOSIK SEGMENT
db 100h dup (?)
STOSIK ENDS
DANE ENDS

END Start;koniec programu z zaznaczonym miejscem startu programu


